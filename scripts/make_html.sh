#!/usr/bin/env bash

if [[ $# -lt 1 ]]; then
  echo "Usage: ./make_html file.md [file.html]"
fi

input="$1"
dir=$(dirname "$input")
name=$(basename "${input%.md}")
[[ -z "$2" ]] && output="$dir/${name}.html" || output="$2"

read -d '' html << EOF
<!doctype html>
<html>
<title>Introduction to Machine Knitting | $title</title>

<xmp theme="simplex">
$(cat "$input")
</xmp>
<script src="sd/strapdown.js"></script>
</html>
EOF

echo "$html" > "$output"
